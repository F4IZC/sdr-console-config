# SDR Console config files



## Frequency Markers

In SDR Console : View > Markers > Configuration. In Marker Configuration window choose XML : Import and select Markers file

## Custom Layout

Copy the custom layout file in C:\Users\USER\AppData\Roaming\SDR-RADIO.com (V3)\Console\Ident0

In SDR Console Layout > Custom section > Refresh and select the custom layout.
